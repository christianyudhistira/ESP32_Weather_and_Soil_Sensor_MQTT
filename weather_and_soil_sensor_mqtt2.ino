#include <OneWire.h>
#include <DallasTemperature.h>
#include <WiFi.h>
#include <PubSubClient.h>

#define pinSoilHumidity 36
#define pinRainDrop 39
#define pinSoilPH 32
#define pinSoilTemperature 17
#define pinAirTemperature 5

/* change it with your ssid-password */
const char* ssid = "mikrotik";
const char* password = "baliteam888";
/* create an instance of PubSubClient client */
WiFiClient espClient;
PubSubClient client(espClient);
int status = WL_IDLE_STATUS;

// define thingsboard components
#define TOKEN "czlslmcx7khI1mDyIIWw"
char thingsboardServer[] = "demo.thingsboard.io";

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWireSoil(pinSoilTemperature);
OneWire oneWireAir(pinAirTemperature);
// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature sensorSuhuSoil(&oneWireSoil);
DallasTemperature sensorSuhuAir(&oneWireAir);

int humiditySoilValue = 0;
int rainDropValue = 0;
float phSoilValue = 0.0;
float phResult = 0.0;
float soilTemperatureValue = 0;
float airTemperatureValue = 0;

int lastSend = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  // init sensor
  pinMode(pinSoilHumidity, INPUT);
  pinMode(pinRainDrop, INPUT);
  pinMode(pinSoilPH, INPUT);
  sensorSuhuSoil.begin();
  sensorSuhuAir.begin();

  // init WiFi
  Serial.print("Connecting to WiFi");
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // init mqtt server
  client.setServer( thingsboardServer, 1883 );
}

void loop() {
  // put your main code here, to run repeatedly:
  if ( !client.connected() ) {
    reconnect();
  }

  if ( millis() - lastSend > 1000 ) { // Update and send only after 1 seconds
    getAndSendData();
    lastSend = millis();
  }

  client.loop();
}

void getAndSendData() {
  // Prepare a JSON payload string
  String payload = "{";

  // kelembaban tanah
  int tempHumidityValue = analogRead(pinSoilHumidity);
  humiditySoilValue = (4095-tempHumidityValue)*100/4095;
  payload += "\"Kelembaban Tanah\":"; payload += String(humiditySoilValue); payload += ",";

  // rain drop
  int tempRainDropValue = analogRead(pinRainDrop);
  rainDropValue = (4095-tempRainDropValue)*100/4095;
  payload += "\"Rain Drop\":"; payload += String(rainDropValue); payload += ",";

  // pH Tanah
  phSoilValue = analogRead(pinSoilPH);
  phResult = 10*(phSoilValue*(-0.0693) + 41.3855);
  if (phResult<57)
    phResult = 57;
  else if (phResult > 70)
    phResult = 70;
  payload += "\"pH Tanah\":"; payload += String(phResult); payload += ",";

  // Suhu Tanah
  sensorSuhuSoil.requestTemperatures(); 
  soilTemperatureValue = sensorSuhuSoil.getTempCByIndex(0);
  payload += "\"Suhu Tanah\":"; payload += String(soilTemperatureValue); payload += ",";

  // Suhu Udara
  sensorSuhuAir.requestTemperatures(); 
  airTemperatureValue = sensorSuhuAir.getTempCByIndex(0);
  payload += "\"Suhu Udara\":"; payload += String(airTemperatureValue);

  // close JSON payload string
  payload += "}";

  // Send payload
  char attributes[400];
  payload.toCharArray( attributes, 400 );
  client.publish( "v1/devices/me/telemetry", attributes );
  Serial.println( attributes );
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    status = WiFi.status();
    if ( status != WL_CONNECTED) {
      WiFi.begin(ssid, password);
      while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
      }
      Serial.println("Connected to AP");
    }
    Serial.print("Connecting to Thingsboard node ...");
    // Attempt to connect (clientId, username, password)
    if ( client.connect("ESP32 Device", TOKEN, NULL) ) {
      Serial.println( "[DONE]" );
    } else {
      Serial.print( "[FAILED] [ rc = " );
      Serial.print( client.state() );
      Serial.println( " : retrying in 5 seconds]" );
      // Wait 5 seconds before retrying
      delay( 5000 );
    }
  }
}
